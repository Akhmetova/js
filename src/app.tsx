import React from 'react';
import Home from './pages/home';
import { BrowserRouter } from "react-router-dom";

import './app.css';
import Grid from './containers/grid';

const App = () => (
    <BrowserRouter>
        <Home />
    </BrowserRouter>
);

export default App;
