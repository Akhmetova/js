import React from 'react';
import {
    Switch,
    Route,
    Redirect,
} from "react-router-dom";

import { URLs } from '../__data__/urls';
import LazyComponent from '../components/lazy-component';

import Login from './auth/login/login';
import Registration from './auth/register';
import { Recoverable } from 'repl';
import Register from './auth/register';

const Dashboard = () => (
    <Switch>
        <Route exact path={URLs.root.url}>
            <Redirect to={URLs.login.url} />
        </Route>
        <Route path={URLs.login.url}>
            <LazyComponent>
                <Login />
            </LazyComponent>
        </Route>
        <Route path={URLs.register.url}>
            <LazyComponent>
                <Register />
            </LazyComponent>
        </Route>
        <Route path={URLs.recoverPassword.url}>
            <LazyComponent>
                <h1> Recover Password</h1>
            </LazyComponent>
        </Route>
        <Route path="*">
            <h1>Not Found</h1>
        </Route>
    </Switch>
)

export default Dashboard;

