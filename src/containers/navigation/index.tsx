import React from 'react';
import { Link as ConnectedLink, 
    Redirect,
    Switch,
    Route
} from "react-router-dom";
import { render } from 'react-dom';
import Modal from './../../components/modal';
import Login from '../auth/login/login';
import {
    Button,
    ButtonColorScheme,
    Link,
    LinkColorScheme,
} from '../../components';
import { URLs } from './../../__data__/urls';
import Grid from '../../containers/grid';
import i18next from 'i18next';
import style from './style.css';

interface NavigationProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{    

}

class Navigation extends React.PureComponent<{}, {visible:boolean}> {


constructor(props) {
    super(props);
    this.state = {visible: false};
}
handleModalShow = () => {
    this.setState({visible: true});
    //this.firstInputRef.current.focus();
}
handleModalClose = () => {
        this.setState({visible: false});
}

render() {
    console.log(URLs);
        return(
            <nav>
                <div className={style.nav}>
                    <Link
                        type="button"
                        className ={style.item}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        { i18next.t('js.navigation.main') }
                    </Link>
                    <Link
                        type="button"
                        className ={style.item}
                        to={ URLs.login.url }
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        { i18next.t('js.navigation.auth') }
                    </Link>
                    <Link
                        type="button"
                        className ={style.item}
                        to={'#'}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        { i18next.t('js.navigation.smlab') }
                    </Link>
                    <Link
                        type="button"
                        className ={style.item}
                        to={'#'}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        { i18next.t('js.navigation.ckQa') }
                    </Link>
                    <Link
                        type="button"
                        className ={style.item}
                        to={ URLs.register.url }
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        { i18next.t('js.navigation.register') }
                    </Link>
                    <Link
                        type="button"
                        className ={style.item}
                        to={URLs.register.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        { i18next.t('js.navigation.ipr') }
                    </Link>
                </div>
            </nav> 
        )
    }
}

export default Navigation
