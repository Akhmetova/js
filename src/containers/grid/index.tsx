import React from 'react';
import Bar from '../bar';
import Main from '../main';
import Header from '../header';
import Navigation from '../navigation';
import style from './style.css';

class Grid extends React.PureComponent {
   render() {
       return (
        <div className={style.gridTemplate}>
            <div className={style.header}><Header /></div>
            <div className={style.nav}><Navigation /></div>
            <div className={style.bar}><Bar /></div>
            <div className={style.footer}>FOOTER</div>
            <div className={style.common}><Main /></div>
        </div>
       )
   } 
}
export default Grid