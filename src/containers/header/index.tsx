import React from 'react';
import UserIcon from '../../components/user-icon';
import { iconURL, Icon } from '../../assets';


import Searching from '../../components/searching';
import style from './style.css';


interface HeaderProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{    
    
}

const Header : React.FC<HeaderProps> =(props) => {

    const {children} = props;
    return ( 
        <header>
            <div className={style.wrapper}>
                <div>
                    <img src={iconURL} className={ style.logo } alt="logo" />
                </div>
                <div><Searching /></div>
                <div><UserIcon /></div>
            </div>
        </header>
    )
}

export default Header;