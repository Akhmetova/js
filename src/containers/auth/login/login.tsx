import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink, Redirect } from "react-router-dom";
import { URLs } from '../../../__data__/urls';
import Modal from '../../../components/modal';

import {
    Button,
    ErrorBoundary,
    ButtonColorScheme,
    Input,
    Link,
    LinkColorScheme
} from '../../../components';

import {Size} from '../../../__data__/model';

import style from './../../form/style.css';
import { url } from 'inspector';
import Header from './../../../containers/header';

function useInputValue(defaultValue =''): [string, (event: any) => void ]{
    const [value, setValue] = useState<string>(defaultValue);
    
    function handleInputChange(event){
        setValue(event.target.value);   
    }

    return [value, handleInputChange]
}

function Login(){

    const loginRef = useRef(null);
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    function handleInputChange(event){
        setLogin(event.target.value);   
    }

    useEffect(() => {
        loginRef.current.focus();
    }, [])
    return(
        <React.Fragment>
        <Header />
        <div className={style.form}>
            <h4 className={style.title}>Авторизация</h4>
                <Input
                    className={style.input}
                    inputRef={loginRef}
                    label="Логин"
                    id="login-input"
                    name="login"
                    value={login}
                    onChange={handleInputChange}
                    />                            
                <Input 
                    className={style.input}
                    label = "Пароль" 
                    id="password-input" 
                    link={{
                        href: URLs.recoverPassword.url,
                        label: 'Забыли пароль?'
                    }}
                    name="password" 
                    type="password" 
                    value={password}
                    onChange={setPassword}
                />
                <Button
                    type="submit"
                    className={style.submitButton} 
                    colorScheme={ButtonColorScheme.red}
                >
                        Войти
                </Button>
                <Link
                    type="link"
                    to={URLs.register.url}
                    colorScheme={LinkColorScheme.blue}
                    as={ConnectedLink}
                >
                    Зарегистрироваться
                </Link>
        </div>
    </React.Fragment>
    )
}
export default Login;