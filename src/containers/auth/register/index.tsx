import { lazy } from 'react';

export default lazy(() => import(/* webpackChunkName: "reg-page" */ './register'));