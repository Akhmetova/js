import React from 'react';

import Input from '../../../components/input';
import { Link } from '../../../components/link';
import { URLs } from '../../../__data__/urls';
import { Link as ConnectedLink } from 'react-router-dom';

import style from './../../form/style.css';
import Header from '../../../containers/header';
import { Button, ButtonColorScheme } from './../../../components';

class Register extends React.Component {
    firstInputRef = React.createRef<HTMLInputElement>();
  
    componentDidMount() {
      this.firstInputRef.current.focus();
    }
  
    render() {
      return (
        <React.Fragment>
        <Header />
          <main className={style.container}>
            <div className={style.form}>
                 <form className="" method="post" action="#">
                    <h4 className={style.title}>Регистрация</h4>
                    <Input inputRef={this.firstInputRef} label="ФИО" id="username" name="username" type="text" placeholder="Elliot Page" />
                    <Input label="Почта" id="userEmail" name="email" type="email" placeholder="email@example.com" />
                    <Input label="Пароль" id="password" name="password" type="password" placeholder="••••••" />
                    <Input label="Повторите пароль" id="repeatPassword" name="repeatPassword" type="password" placeholder="••••••" />
                    <span>
                    <Button
                    type="submit"
                    className={style.submitButton} 
                    colorScheme={ButtonColorScheme.red}
                    >
                            Отправить
                    </Button>
                        <Link
                            type="link"
                            to={URLs.login.url}
                            as={ConnectedLink}
                        >
                            Авторизация
                        </Link>
                    </span>
                    <span>
                        <Link
                            type="link"
                            to={URLs.root.url}
                            as={ConnectedLink}
                        >
                            Отмена
                        </Link>
                    </span>
                </form>
            </div>    
        </main>
        </React.Fragment>
      );
    }
  }
  
  export default Register;
  