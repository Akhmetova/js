import React from 'react';
import Bar from '../bar';
import { URLs } from './../../__data__/urls';
import { Link as ConnectedLink, 
    Redirect,
    Switch,
    Route
} from "react-router-dom";
import { render } from 'react-dom';
import {
    Button,
    ButtonColorScheme,
    Link,
    LinkColorScheme,
} from '../../components';
import i18next from 'i18next';
import style from './style.css';


const Main = () => {
    return(
       <main>
            <article>
                <section>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.main') }
                        </span>
                    </Link>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.ckAnalisys') }
                        </span>
                    </Link>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.ckDev') }
                        </span>
                    </Link>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.ckQa') }
                        </span>   
                    </Link> 
                </section> 
                <section>
                <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.ipr') }
                        </span>
                    </Link>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.info') }
                        </span>
                    </Link>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.ckDev') }
                        </span>
                    </Link>
                    <Link
                        type="button"
                        className ={style.row}
                        to={URLs.root.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        <span className={style.item}>
                            { i18next.t('js.navigation.ckQa') }
                        </span>   
                    </Link>
                </section>
            </article> 
       </main>
    ) 
};

export default Main;