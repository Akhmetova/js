import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('js');

export const URLs = {
    root: {
        url: navigations['js']
    },
    login: {
        url: navigations['link.js.login'],
    },
    register: {
        url: navigations['link.js.register'],
    },
    recoverPassword: {
        url: navigations['link.js.recover.password'],
    },
}