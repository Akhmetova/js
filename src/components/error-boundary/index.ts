import React from 'react'

import ErrorComponent from '../error'

class ErrorBoundary extends React.Component {
    state = {
        isError: false,
    }

    static getDerivedStateFromError () {
        return {
            isError: true
        }
    }

    render() {
        const { isError } = this.state

        if(isError) {
            //TODO
            //return <ErrorComponent />
            return null
        }

        return this.props.children
    }
}

export default ErrorBoundary
