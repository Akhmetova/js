import React from 'react';
import style from './style.css';

interface ModalProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{
    visible: boolean;
    onClose?: ()=> void;
}

const Modal : React.FC<ModalProps> =(props) => {
    const {children, visible} = props;
    if (!visible){
        return null;
    }
        return <div onClick={props.onClose}>
                    <h1 className={style.modal} >{children}</h1>
                </div>

}

export default Modal;