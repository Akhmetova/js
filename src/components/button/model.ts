import Button from "./button";

export enum ButtonColorScheme{
    red = 'red',
    blue = 'blue'
}