import React from 'react';
import cn from 'classnames';

import { ButtonColorScheme} from './model'
import style from './style.css';

interface ButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{
    colorScheme: ButtonColorScheme;
    onClick?: () => void;
    className?: string;
}

const Button: React.FC<ButtonProps> = ({ colorScheme, children, onClick, className, ...rest}) => (
    <button
        {...rest}
        type="button"
        onClick ={onClick}
        className={cn(style.main, style[colorScheme], className)}
    >
        {children}
    </button>
)

Button.defaultProps = {
    colorScheme: ButtonColorScheme.blue,
}

export default Button;