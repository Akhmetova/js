import React from 'react';
import { render } from 'react-dom';
import {
    Input
} from '../index';

import style from './style.css';

//import { Logo } from '../../assets';

interface SearchingProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{    

}

class Searching extends React.PureComponent<{}, {visible:boolean}> {

    constructor(props) {
        super(props);
        this.state = {visible: false};
    }

    render() {
        return(
        <div className={style.searching}>
            <input 
                className={style.input}
                id="searching"
                name="searching"
                placeholder="поиск"
            />
        </div>
        )
    }
}

export default Searching