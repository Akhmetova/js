import React from 'react';
import cls from 'classnames';

import { Size } from '../../__data__/model';

import style from './style.css';

export enum LinkColorScheme {
    blue = 'blue',
    black = 'black',
}

type LinkProps = {
    size?: Size;
    to: string;
    colorScheme?: LinkColorScheme;
    className?: string;
    type?: 'link' | 'button';
    as?: any;
};

export type LinkType = React.FC<LinkProps>;

export const Link: LinkType = ({
    type,
    size,
    to,
    children,
    colorScheme,
    className,
    as: LinkComponent
}) => {
    return (
        <LinkComponent
            className={cls(
                style.commoon,
                style[`type-${type}`],
                style[`color-${colorScheme}`],
                style[`size-${size}`],
                className
            )}
            to={to}
        >
            {children}
        </LinkComponent>
    )
}

Link.defaultProps = {
    size: Size.md,
    colorScheme: LinkColorScheme.blue,
    type: 'link',
    as: 'a'
}
