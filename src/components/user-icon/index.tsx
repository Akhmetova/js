import React from 'react';
import style from './style.css';

import {
    uIcon,
    userIconURL
} from './../../assets';

interface UserIconProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{    
    
}

const UserIcon : React.FC<UserIconProps> =(props) => {

    const {children} = props;
    return ( 
        <div className={style.icon}>
            <img src={ userIconURL } />
        </div>
    )
}

export default UserIcon;
