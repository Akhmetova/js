import React from 'react';
import cls from 'classnames';
import { Link as ConnectedLink } from "react-router-dom";

import { Link } from '../../__data__/model';

import style from './style.css'
import { FieldRenderProps } from 'react-final-form';

interface Props {
    id: string | number;
    name: string;
    label?: string;
    type?: string;
    link?: Link;
    inputRef?: React.RefObject<HTMLInputElement>;
    error?: string | boolean;
}

type InputProps = Props & Omit<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>, 'id'>;

export const Input: React.FC<Partial<FieldRenderProps< InputProps>>> = ({ inputRef, className, label, id, link, name, type, error,input, meta, ...rest }) => (
    <div className={cls(style.wrapper, className)}>
        <div className={style.infoBlock}>
            {label && <label className={style.label} htmlFor={String(id)}>{label}</label>}
            {link && <ConnectedLink className={style.link} to={link.href}>{link.label}</ConnectedLink>}
            {((error && typeof error !== 'boolean') || ((meta?.error || meta?.submitError) && meta.touched)) && <div className={style.error}><span>{(meta?.error || meta?.submitError) || error}</span></div>}
        </div>

        <input {...input} value={String(input?.value) || rest.value || ''} 
        className={cls(style.field, (((meta?.error || meta?.submitError)&& meta.touched) || error) && style.alert)} ref={inputRef} 
        type={type} name={name} id={String(id)} {...rest} />
    </div>
)

Input.defaultProps = {
    type: 'text',
    error: void 0,
}
