import React, { Suspense } from 'react';
import {
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";

import { URLs } from '../../__data__/urls';

import style from './style.css';
import Grid from '../../containers/grid';
import Login from '../../containers/auth/login/login';
import Register from '../../containers/auth/register';

const Home = () => (
    <Switch>
        <Route exact path={ URLs.root.url }>
            <Grid />
        </Route>
        <Route path={ URLs.login.url }>
            <Login />
        </Route>
        <Route path={URLs.register.url }>
        <Suspense fallback="Загрузка...">
            <Register />    
        </Suspense>    
            
        </Route>
        <Route path={URLs.recoverPassword.url}>
            <h1>Recover Password</h1>
        </Route>
        <Route path="*">
            <h1>Not found</h1>
        </Route>
    </Switch>
)

export default Home;