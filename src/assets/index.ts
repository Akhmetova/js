import iconURL, { ReactComponent as Icon } from './icons/logo.svg';
import userIconURL, { ReactComponent as uIcon } from './icons/user-logo.svg';

export {
    iconURL,
    Icon,
    userIconURL,
    uIcon
}
